<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('endness_department', 'ToolController@endness_department');
Route::get('calc_money', 'ToolController@calc_money');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/calcExpression', 'AlgorithmController@calcExpression');
Route::get('/maxSubqueue', 'AlgorithmController@maxSubqueue');
Route::get('/getStackMin', 'AlgorithmController@getStackMin');
Route::get('/gcd', 'AlgorithmController@gcd');
Route::get('/linklist', 'AlgorithmController@linklist');
Route::get('/searchTree', 'AlgorithmController@searchTree');
Route::get('/kmp', 'AlgorithmController@kmp');