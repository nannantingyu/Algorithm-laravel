<?php
namespace App\Libs;

class Rsa {
    public $public_key = "";
    public $private_key = "";
    public $key_length = "";
    public $modulus = "";
    public $public = "";

    function __construct() {
        //########################
        //注册自动加载类
        $autoload = function($className) {
            if (strpos($className, "\\") === false) {
                $className = __NAMESPACE__ . "\\" . $className;
            }
            $class_arr = explode("\\", $className);
            if (count($class_arr) >= 2) {
                $class_arr1 = array_slice($class_arr, 0, -1);
                $class_arr2 = array_slice($class_arr, count($class_arr) - 1);
                $class_arr = array_merge($class_arr1, ["Rsa"], $class_arr2);
                $classPath =  'Rsa'. join($class_arr, "/") . ".php";
            }
            if (file_exists($classPath)) {
                include_once($classPath);
            }
        };
        spl_autoload_register($autoload);
        //#########################
        $this->public_key = "00d70b87ffe23d15d17068d785b5a86ca350ca79d8866a365091d7a4e577d7a21561c779d9c7ad140bdc0e100485e6a1441545";
        $this->private_key = "716305974646676130933867879024172992983760771286092485415718958354174692845910256622874746661654275402665350314485540801";
        $this->key_length = "400";
        $this->modulus = "2169140775363608427733707568043952751186430536354155956782646578236725354919491489624192018445925569164856033629977187653";
        $this->public = "65537";
        //#########################
    }

    function ShowJs($data = "") {
        $packer = new JavaScriptPacker(stripcslashes($data));
        $result = $packer->pack();
        return $result;
    }

    function public_key() {
        return $this->public_key;
    }

    function decode($code) {
        $code = trim($code);
        if ($code == "") {
            return "";
        }
        $encrypted = $this->convert($code); //hex data to bin data
        $R = new Rsas();
        return $R->rsa_decrypt($encrypted, $this->private_key, $this->modulus, $this->key_length);
    }

    function convert($hexString) {
        $hexLenght = strlen($hexString);
        // only hex numbers is allowed 
        if ($hexLenght % 2 != 0 || preg_match("/[^\da-fA-F]/", $hexString))
            return FALSE;
        unset($binString);
        for ($x = 1; $x <= $hexLenght / 2; $x++) {
            @$binString .= chr(hexdec(substr($hexString, 2 * $x - 2, 2)));
        }
        return $binString;
    }
}
