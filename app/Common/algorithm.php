<?php

use App\Support\Stack;

/**
 * 中缀表达式转后缀表达式
 * @param String $expression 中缀表达式
 */
function infixToSuffix(String $expression) {
    $stack = new Stack();
    $ex_cal = [];

    $calcPriority = [
        '+' => 1,
        '-' => 1,
        '*' => 2,
        '/' => 2,
        '(' => 0,
        ')' => 0
    ];

    $expression_split = splitExpression($expression);

    foreach ($expression_split as $ex) {
        if(isset($calcPriority[$ex])) {
            $topEx = $stack->seek();
            if(is_null($topEx) or $ex === '(') {
                $stack->push($ex);
            }
            elseif($ex == ')') {
                while($stack->seek() !== '(') {
                    array_push($ex_cal, $stack->pop());
                }

                array_push($ex_cal, $stack->pop());
                array_push($ex_cal, $ex);
            }
            else {
                while(!$stack->is_empty() and $calcPriority[$stack->seek()] >= $calcPriority[$ex]) {
                    array_push($ex_cal, $stack->pop());
                }

                $stack->push($ex);
            }
        }
        else {
            array_push($ex_cal, $ex);
        }
    }

    while(!$stack->is_empty()) {
        array_push($ex_cal, $stack->pop());
    }

    return $ex_cal;
}

/**
 * 计算后缀表达式
 * @param array $ex
 */
function calcSuffixExpression($expression) {
    $stack = new Stack();

    $ex = infixToSuffix($expression);
    foreach ($ex as $val) {
        if(numcheck::is_float($val)) {
            $stack->push($val);
        }
        elseif($val != '(' and $val != ')') {
            $num1 = $stack->pop();
            $num2 = $stack->pop();

            eval("\$re = $num2 $val $num1 ;");
            $stack->push($re);
        }
    }

    return $stack->pop();
}

/**
 * 将字符串表达式分割成数组
 * @param $expression
 * @return array
 */
function splitExpression($expression) {
    $ex = [];

    $num_now = "";
    for($index = 0, $max = strlen($expression); $index < $max; $index ++) {
        $ex_now = $expression[$index];

        if($ex_now === ' ') {
            continue;
        }
        elseif($ex_now === '.' or numcheck::is_int($ex_now)) {
            $num_now .= $ex_now;
        }
        else {
            if($num_now !== "") {
                array_push($ex, $num_now);
                $num_now = "";
            }

            array_push($ex, $ex_now);
        }
    }

    if($num_now !== "") {
        array_push($ex, $num_now);
    }

    return $ex;
}

/**
 * 计算最大子序列和, 算法复杂度O(N^3)
 * @param $list
 * @return int
 */
function maxSubQueue1($list) {
    $maxSum = 0;
    for($i = 0; $i < count($list); $i ++) {
        for($j = $i; $j < count($list); $j ++) {

            $thisSum = 0;
            for($k = $i; $k <= $j; $k ++) {
                $thisSum += $list[$k];
            }

            if($thisSum > $maxSum) {
                $maxSum = $thisSum;
            }
        }
    }

    return $maxSum;
}

/**
 * 计算最大子序列和, 算法复杂度O(N^2)
 * @param $list
 * @return int
 */
function maxSubQueue2($list) {
    $maxSum = 0;
    for($i = 0; $i < count($list); $i ++) {
        $thisSum = 0;
        for($j = $i; $j < count($list); $j ++) {
            $thisSum += $list[$j];

            if($thisSum > $maxSum) {
                $maxSum = $thisSum;
            }
        }
    }

    return $maxSum;
}

/**
 * 分治法查找最大子序列和
 * @param $list
 * @param $start
 * @param $end
 * @return int|mixed
 */
function maxSubQueue3($list, $start, $end) {
    $length = $end - $start + 1;
    if($length == 1) {
        return $list[$start] > 0?$list[$start]: 0;
    }

    $middle = floor(($start + $end) / 2);
    $leftMaxSubQueue = maxSubQueue3($list, $start, $middle);
    $rightMaxSubQueue = maxSubQueue3($list, $middle+1, $end);
    $middleLeftMax = 0;
    $middleRightMax = 0;
    $middleLeftSum = 0;
    $middleRightSum = 0;
    for($index = $middle; $index >= $start; $index --) {
        $middleLeftSum += $list[$index];
        if($middleLeftSum > $middleLeftMax) {
            $middleLeftMax = $middleLeftSum;
        }
    }

    for($index = $middle+1; $index <= $end; $index ++) {
        $middleRightSum += $list[$index];
        if($middleRightSum > $middleRightMax) {
            $middleRightMax = $middleRightSum;
        }
    }

    return max($leftMaxSubQueue, $rightMaxSubQueue, $middleLeftMax+$middleRightMax);
}

/**
 * 计算最大子序列和, 算法复杂度O(N)
 * @param $list
 * @return int
 */
function maxSubQueue4($list) {
    $maxSum = 0;
    $thisSum = 0;

    for($i = 0; $i < count($list); $i ++) {
        $thisSum += $list[$i];

        if($thisSum > $maxSum) {
            $maxSum = $thisSum;
        }

        if($thisSum < 0) {
            $thisSum = 0;
        }
    }

    return $maxSum;
}

/**
 * 生成随机数组
 * @param $length
 * @return array
 */
function generateRandomArray($length) {
    $array = [];
    for($index = 0; $index < $length; $index ++) {
        array_push($array, random_int(-100, 100));
    }

    return $array;
}

/**
 * 计算执行方法的时间
 */
function count_time($func, $args){
    $start = time();
    if(is_array($func)) {
        $result = call_user_func_array($func, $args);
    }
    else {
        $result = call_user_func($func, ...$args);
    }

    $end = time();
    dump('方法【'.$func.'】计算，共用时：'. ($end-$start). '秒. 结果是：' . $result);
}

/**
 * 获取两个数的最大公约数
 * @param $num1
 * @param $num2
 */
function gcd($num1, $num2) {
    if($num1 == $num2) {
        return $num1;
    }

    if($num1 < $num2) {
        return gcd($num2, $num1);
    }
    else {
        if(!($num1 & 1) && !($num2 & 1)) {
            return gcd($num1 >> 1, $num2 >> 1) << 1;
        }
        elseif(!($num1 & 1) && ($num2 & 1)) {
            return gcd($num1 >> 1, $num2);
        }
        elseif(($num1 & 1) && !($num2 & 1)) {
            return gcd($num1, $num2 >> 1);
        }
        elseif(($num1 & 1) && ($num2 & 1)) {
            return gcd($num2, $num1-$num2);
        }
    }
}

/**
 * 计算字符串的部分匹配表
 * @param $string
 * @param $i
 */
function kmp($string, $i) {
    $j = 0;
    $k = $i;
    while($j <= $k) {
        if($string[$k] === $string[$j])
        $i --;
        $j ++;
    }

    return $j;
}

function getKmpNext($string) {
    $strlen = strlen($string);
    $i = 0;
    $j = - 1;


    $next[0] = -1;

	while ($i < $strlen)
    {
        if ($j == -1 || $string[$i] == $string[$j])
        {
            ++$i;
            ++$j;
            $next[$i] = $j;
        }
        else
            $j = $next[$j];
    }

    return $next;
}