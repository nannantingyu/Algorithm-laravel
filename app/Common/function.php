<?php
use Illuminate\Support\Facades\Redis;

function getv_from_r($key) {
    return Redis::get($key);
}

/**
 * curl获取接口数据
 * @param $url 链接地址
 * @return mixed
 */
function https_request($url, $data = null){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    if (!empty($data)){
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);

    try{
        $ret = json_decode($output, true);
    }
    catch (Exception $e) {
        $ret = $output;
    }

    return $ret;
}

/**
 * 获取小程序二维码
 * @param $url
 * @param $post_data
 * @param $filename
 * @return string
 */
function saveQrcode($url, $post_data, $filename) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $tmpInfo = curl_exec($ch);

    $filepath = dirname($filename);
    if(!file_exists($filepath)) {
        mkdir($filepath);
    }

    file_put_contents($filename, $tmpInfo);

    return config("app.url").$filename;
}

/**
 * 链接跳转
 * @param $url
 * @return \Illuminate\Http\RedirectResponse
 */
function redirectToUrl($url) {
    return response()->redirectTo(config("app.url_prefix").$url);
}


/**
 * 数据验证
 * Class numcheck
 */
class numcheck{
    static function is_float($str) {
        $reg = '/^-?\d+(\.\d+)?$/';
        $result = static::check($reg, $str);
        return $result;
    }

    static function is_int($str) {
        $reg = '/^-?\d+$/';
        $result = static::check($reg, $str);
        return $result;
    }

    private static function check($reg, $str) {
        preg_match($reg, $str, $match);
        if(count($match) > 0) {
            return true;
        }

        return false;
    }
}