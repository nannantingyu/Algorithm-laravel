<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class ToolController extends Controller
{
    public function endness_department(Request $request) {
        $departments = DB::table('department')
            ->get()->toArray();

        $departments = $this->obj_to_array($departments);
        $tree = $this->lists_to_tree($departments, 0);
        dump($tree);

        $find_child = $this->get_child_department($tree, 5);
        dump($find_child);

        $find_parents = $this->get_parents_recursion($departments, 8);
        dump($find_parents);
    }

    /**
     * 创建部门的树形结构
     * @param $lists
     * @param $pid
     * @return array
     */
    public function lists_to_tree($lists, $pid) {
        $tree = [];

        foreach ($lists as $list) {
            if($list['parent_id'] == $pid) {
                $list['_child'] = $this->lists_to_tree($lists, $list['id']);
                $tree[] = $list;
            }
        }

        return $tree;
    }

    /**
     * 获取所有的子部门
     * @param $tree
     * @param $id
     * @return mixed
     */
    public function get_child_department($tree, $id) {
        foreach ($tree as $val) {
            if($val['id'] == $id) {
                return $val;
            }

            return $this->get_child_department($val['_child'], $id);
        }
    }


    /**
     * 根据pid获取所有的父部门
     * @param $lists
     * @param $pid
     * @return array
     */
    public function get_parents_recursion($lists, $pid) {
        static $step = 0;
        $parents = [];
        foreach ($lists as $val) {
            $step += 1;
            if($val['id'] == $pid) {
                $parents = $val;
                $parents["_parent"] = $this->get_parents_recursion($lists, $val['parent_id']);
            }
        }

        return $parents;
    }

    /**
     * Laravel对象转为数组
     * @param $obj
     * @return mixed
     */
    public function obj_to_array($obj){
        return json_decode(json_encode($obj), true);
    }

    /**
     * 将整数每3位添加一个','
     * @param $money
     * @return int|string
     */
    private function calc_int($money) {
        $money_calc = "";

        while($money > 0) {
            $money_left = $money % 1000;
            $money = floor($money / 1000);
            if($money > 0) {
                $money_left = str_pad($money_left, 3, '0', STR_PAD_LEFT );
            }

            $money_calc = empty($money_calc)?$money_left:$money_left . "," . $money_calc;
        }

        return $money_calc;
    }

    /**
     * 将整数或者小数每3位添加一个','
     * @param Request $request
     * @return int|string
     */
    public function calc_money(Request $request){
        $money = $request->input('money');

        if(!\numcheck::is_float($money)) {
            return "数字不合规范";
        }

        if(strpos($money, ".") !== false) {
            $money = explode(".", $money);
            list($integer, $decimal) = $money;

            $integer_calc = $this->calc_int($integer);
            $decimal_calc = $this->calc_int(strrev($decimal));

            return $integer_calc.'.'.strrev($decimal_calc);
        }

        return $this->calc_int($money);
    }
}
