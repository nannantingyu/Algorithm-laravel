<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Support\MinStack;
use App\Support\LinkList;
use App\Support\SearchTree;

class AlgorithmController extends Controller {

    /**
     * 计算表达式结果
     * @param Request $request
     */
    public function calcExpression(Request $request) {
        $expression = $request->input('expression', '12+48/(6-9+4)*3+26');
        $ex_cal = calcSuffixExpression($expression);
        print($expression . ' = '. $ex_cal);
    }

    /**
     * 计算最大子序列和, 算法复杂度O(N^3)
     * @param Request $request
     */
    public function maxSubqueue(Request $request) {
        $queue = explode(",", $request->input('queue', '1,2,3,4,5,6,7'));
        $type = $request->input('type', 1);
        $random = $request->input('random', 0);
        if(\numcheck::is_int($random) && $random > 0) {
            $queue = generateRandomArray($random);
        }

        $max = 0;
        switch ($type) {
            case 1:
                $max = maxSubqueue1($queue);
                break;
            case 2:
                $max = maxSubqueue2($queue, 0, count($queue)-1);
                break;
            case 3:
                $max = maxSubqueue3($queue);
                break;
            case 4:
                $max = maxSubqueue4($queue);
                break;
            case 5:
                count_time('maxSubqueue1', [$queue]);
                count_time('maxSubqueue2', [$queue]);
                count_time('maxSubqueue3', [$queue, 0, count($queue)-1]);
                count_time('maxSubqueue4', [$queue]);
                break;
            default:
                $max = maxSubqueue4($queue);
                break;
        }

        return $max;
    }

    /**
     * 可获取最小值的栈
     * @param Request $request
     */
    public function getStackMin(Request $request) {
        $stack = new MinStack([1, 5, 8, 2, 3, -2, 9]);
        $stack->travel();

        dump($stack->get_min());
        $stack->push(3);
        dump($stack->get_min());
        $stack->push(-6);
        dump($stack->get_min());
        $stack->push(2);
        dump($stack->get_min());

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());
        $stack->get_min_stack()->travel();

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());
        $stack->get_min_stack()->travel();

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());


        $stack->pop();
        $stack->travel();
        dump($stack->get_min());

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());

        $stack->pop();
        $stack->travel();
        dump($stack->get_min());
    }

    /**
     * 获取最大公约数
     * @param Request $request
     */
    public function gcd(Request $request) {
        $num1 = $request->input('num1', 25);
        $num2 = $request->input('num2', 15);
        return gcd($num1, $num2);
    }

    /**
     * 单链表
     * @param Request $request
     */
    public function linklist(Request $request) {
        $list = new LinkList();
        $list->createListRear([1,4,2,4,5,35,6]);
        $list->travel();

        $data = $list->getNode(0);
        dump($data);

        $list->insertListPre(3, 10);
        $list->travel();

        $list->insertListAfter(4, 12);
        $list->travel();

        $list->append(22);
        $list->travel();

        $list->delete(3);
        $list->travel();
    }

    /**
     * 二叉搜索树
     * @param Request $request
     */
    public function searchTree(Request $request) {
        $tree = new SearchTree();
        $array = [5, 10, 3, 9, 7, 20, 6, 10, 17, 2, 14, 8];
        foreach ($array as $val)
            $tree->insert($val);

        $treeDg = new SearchTree();
        $array = [10, 3, 9, 7, 20, 6, 5, 12, 11, 15, 8];
        foreach ($array as $val)
            $treeDg->insert($val);

        $tree->middleTravel();
        print("<br>");
        $tree->preTravel();
        print("<br/>");
        $tree->travel();
    }

    /**
     * 计算字符串出现的位置
     * @param Request $request
     */
    public function kmp(Request $request) {
        $string = $request->input('string', 'BBC ABCDAB ABCD  ABCDABDE');
        $substr = $request->input('substr', 'ABCDABD');

        $i = 0; $j = 0;
        $strlen = strlen($string);
        $sublen = strlen($substr);
        $next = getKmpNext($substr);

        while($i < $strlen && $j < $sublen) {
            if($j == -1 || $string[$i] === $substr[$j]) {
                $i ++;
                $j ++;
            }
            else {
                $j = $next[$j];
            }
        }

        if($j == $sublen) {
            return $i - $j;
        }

        return "Not found";
    }
}