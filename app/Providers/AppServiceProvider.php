<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libs\Rsa;
use Illuminate\Support\Facades\Schema;
use App\Support\Stack;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Rsa::class, function($app){
            return new Rsa();
        });
    }
}
