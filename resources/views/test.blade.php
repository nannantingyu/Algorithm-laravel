@extends("common/app")
@section("content")
    <form action="" class="form-horizontal">
        <div class="form-group">
            <label class="col-md-2 control-label" for="">微信号</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="wxid">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-2 col-md-offset-1">
                <img src="{{ $url_prefix }}api/getCaptcha" style="cursor: pointer; height: 80%;" alt="验证码" onclick="$(this).attr('src', '{{ $url_prefix }}api/getCaptcha?'+Math.random());">
            </div>
            <div class="col-md-9">
                <input type="text" class="form-control" id="cap">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="">手机号</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="phone">
            </div>
            <div class="col-md-2">
                <button class="btn btn-default" id="getsms" type="button">获取手机验证码</button>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="">验证码</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="sms">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="">APP</label>
            <div class="col-md-10">
                <select id="app" class="form-control">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2 col-md-offset-4">
                <button class="btn btn-info" id="add" type="button">添加</button>
            </div>
        </div>
    </form>
@endsection("content)
@section("script")
    <script src="/api/rsa"></script>
    <script>
        $('#getsms').click(function(){
            let cap = $('#cap').val(), phone = $('#phone').val();
            if(phone && cap) {
                $.ajax({
                    url: '{{ $url_prefix }}api/getSms',
                    type: 'post',
                    data: {
                        phone: do_encrypt(phone),
                        cap: cap
                    },
                    success: function(data) {
                        if(data.success != 1) {
                            alert(data.msg);
                        }
                        else {
                            alert('手机验证码已发送至您的手机，请注意查收！')
                        }
                    }
                });
            }
            else {
                alert('手机号或验证码不能为空');
            }
        });

        $('#add').click(function(){
           let phone = $('#phone').val(),
               wxid = $('#wxid').val(),
               app = $('#app').val(),
               sms = $('#sms').val();

           if(phone && wxid && app && sms) {
               $.ajax({
                   url: '{{ $url_prefix }}api/postRegist',
                   type: 'post',
                   data: {
                       phone: do_encrypt(phone),
                       wxid: wxid,
                       app: app,
                       sms: sms
                   },
                   success: function(data) {
                       if(data.success == 1) {
                           alert("注册成功！");
                       }
                       else {
                           alert(data.msg);
                       }
                   }
               });
           }
           else {
               alert("信息不完整");
           }
        });
    </script>
@endsection