@extends("common/app")
@section("content")
    <div class="register">
        <p>
            用户ID：<input type="text" class="form-control" name="wxid">
        </p>
        <p>
            <button class="btn btn-default">登录</button>
        </p>
    </div>
@endsection("content)
@section("script")
    <script>
        $("button").click(function(){
            $.ajax({
                url: "{{ $url_prefix }}post_login",
                type: "post",
                data: {
                    wxid: $("input[name='wxid']").val()
                },
                success: function(data) {
                    if(data && data.success == 1) {
                        location.href = "{{ $url_prefix }}register"
                    }
                    else {
                        alert("登录失败");
                    }
                }
            });
        });
    </script>
@endsection