@extends("common/app")
@section("content")
    <ul id="myTab" class="nav nav-tabs">
        <li class="active">
            <a href="#order" data-toggle="tab">当前委托</a>
        </li>
        <li>
            <a href="#history" data-toggle="tab">委托历史</a>
        </li>
        <li><a href="#mine" data-toggle="tab">我的持仓</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade in active" id="order">
            <table class="table table-striped">
                <caption>委托</caption>
                <tr>
                    <th>ID</th>
                    <th>交易方向</th>
                    <th>交易行为</th>
                    <th>购买品种</th>
                    <th>挂单价格</th>
                    <th>交易数量</th>
                    <th>交易总额</th>
                    <th>挂单时间</th>
                    <th>操作</th>
                </tr>
                <tbody id="weituo"></tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="history">
            <table class="table table-striped">
                <caption>委托历史</caption>
                <tr>
                    <th>ID</th>
                    <th>交易方向</th>
                    <th>交易行为</th>
                    <th>购买品种</th>
                    <th>挂单价格</th>
                    <th>交易数量</th>
                    <th>交易总额</th>
                    <th>交易状态</th>
                    <th>挂单时间</th>
                </tr>
                <tbody id="order_history"></tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="mine">
            <p class="text-muted">可用保证金：<span id="availible">0</span></p>
            <table class="table table-striped">
                <caption>我的持仓</caption>
                <tr>
                    <th>ID</th>
                    <th>交易行为</th>
                    <th>品种</th>
                    <th>可用数量</th>
                    <th>冻结数量</th>
                    <th>平均价格</th>
                    <th>上次交易时间</th>
                </tr>
                <tbody id="hold_history"></tbody>
            </table>
        </div>
    </div>
@endsection
@section("script")
    <script>
        var wxid = "{{session('user_id')}}";
        $(function(){
            get_order();
            get_history();
        });

        function cancel(id, obj) {
            $.ajax({
                url: "{{ $url_prefix }}cancelOrder",
                data: {
                    wxid: wxid,
                    id: id
                },
                type: "POST",
                success: function(data) {
                    if(data && data.success) {
                        $(obj).parents("tr").remove();
                        bootbox.alert("撤单成功！");
                    }
                    else {
                        bootbox.alert("撤单失败！");
                    }
                }
            });
        }

        function get_order() {
            $.ajax({
                url: "{{ $url_prefix }}getEntrustDate/" + wxid,
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    html = "";
                    $.each(data, function(index, dt){
                        html += `
                            <tr>
                                <td>${dt['id']}</td>
                                <td>${dt['ocFlag']=='o'?"买入":"卖出"}</td>
                                <td>${dt['bsFlag']==1?"买多":"卖空"}</td>
                                <td>${dt['symbolName']}</td>
                                <td>${dt['price']}</td>
                                <td>${dt['quantity']}</td>
                                <td>${dt['frozenMargin']}</td>
                                <td>${dt['orderTime']}</td>
                                <td><a href="javascript:;" onclick="javascript:cancel(${dt['id']}, this);">撤单</a></td>
                            </tr>
                        `;
                    });

                    $("#weituo").html(html);
                }
            });
        }

        function get_history() {
            $.ajax({
                url: "{{ $url_prefix }}getEntrustDateHistory/" + wxid,
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    html = "";
                    $.each(data, function(index, dt){
                        html += `
                            <tr>
                                <td>${dt['id']}</td>
                                <td>${dt['ocFlag']=='o'?"买入":"卖出"}</td>
                                <td>${dt['bsFlag']==1?"买多":"卖空"}</td>
                                <td>${dt['symbolName']}</td>
                                <td>${dt['price']}</td>
                                <td>${dt['quantity']}</td>
                                <td>${dt['frozenMargin']}</td>
                                <td>${dt['state']==2?"已成交":"已撤销"}</td>
                                <td>${dt['orderTime']}</td>
                            </tr>
                        `;
                    });

                    $("#order_history").html(html);
                }
            });
        }
    </script>
@endsection